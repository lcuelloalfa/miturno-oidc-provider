FROM node:18-alpine

ARG oidc_host=localhost
ARG ui_host=localhost
ENV OIDC_HOST=${oidc_host}
ENV UI_HOST=${ui_host}

WORKDIR /app
COPY package*.json ./

RUN npm install -g nodemon
RUN npm install --quiet

COPY . .

EXPOSE 3000

CMD ash -c "OIDC_HOST=$OIDC_HOST UI_HOST=$UI_HOST nodemon app.js"