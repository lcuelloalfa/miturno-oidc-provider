const Provider = require("oidc-provider");

const { UI_HOST, OIDC_HOST } = process.env;
const OIDC_PORT = 3000;

const server = `http://${OIDC_HOST}:${OIDC_PORT}`;

const oidc = new Provider(server, {
  findAccount(ctx, id) {
    return {
      accountId: id,
      async claims() {
        return {
          sub: "1234567",
          RolUnico: {
            DV: "1",
            numero: 11111111,
            tipo: "RUN",
          },
          name: {
            apellidos: ["Del Río", "Gonzalez"],
            nombres: ["María", "Carmen"],
          },
        };
      },
    };
  },
  claims: {
    openid: ["sub"],
    run: ["RolUnico"],
    name: ["name"],
  },
  clients: [
    {
      client_id: "foo",
      client_secret: "bar",
      redirect_uris: [`http://${UI_HOST}/callback`],
    },
  ],
  formats: {
    AccessToken: "jwt",
  },
  features: {
    introspection: { enabled: true },
    revocation: { enabled: true },
  }
});

oidc.listen(OIDC_PORT);
